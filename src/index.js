import React from 'react';
import ReactDom from 'react-dom';
import Navbar from './Navbar';
import IconImage from './IconImage';


ReactDom.render(<Navbar />, document.getElementById('navbar'));
ReactDom.render(<IconImage />, document.getElementById('iconimage'));
