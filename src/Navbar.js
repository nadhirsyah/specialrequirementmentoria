import React, { Component } from 'react';
import './style.css';

class Navbar extends Component{
    render() {
        return(
            <nav className="navbar navbar-expand-lg navbar-trans navbar-inverse">
                <a className="navbar-brand" href="#">
                    <img src= {"https://i.ibb.co/6vhcVYP/Mentoria.png"} className="imgmentoria" alt="imgmentoria"></img>
                    Mentoria
                </a>
                {/*<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"*/}
                {/*        aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">*/}
                {/*    <span class="navbar-toggler-icon"></span>*/}
                {/*</button>*/}
                <div className="collapse navbar-collapse" id="navbarSupportedContent-4">
                    <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Our Product</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">What Other Says</a>
                            </li>
                    </ul>
                </div>
            </nav>

        )
    }
}

export default Navbar;