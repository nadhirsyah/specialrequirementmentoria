/*eslint-env jquery*/
import React, { Component } from 'react';
import './style.css';
// import './Tooltip.js';

class IconImage extends Component{

    constructor() {
        super();/*
        this.changeBack = this.changeBack.bind(this);
        this.changeNorm = this.changeNorm.bind(this);*/
    }
    componentDidMount() {

        $("#img1").tooltip()
        $("#img2").tooltip()
        $("#img3").tooltip()
        $("#img4").tooltip()
    }
/*
    changeBack(){
        document.getElementById("p2").style.content = '';
        document.getElementById("p2").style.display = 'block';
        document.getElementById("p2").style.position = 'absolute';
        document.getElementById("p2").style.height = '0%';
        document.getElementById("p2").style.width = '100%';
        document.getElementById("p2").style.bottom = '0';
        document.getElementById("p2").style.transition = "height 0.5s ease-out";
        document.getElementById("p2").style.background = "linear-gradient(to bottom, transparent 0%, black 100%)";
    }
    changeNorm(){
        document.getElementById("p2").style.display = '';
        document.getElementById("p2").style.position = '';
        document.getElementById("p2").style.height = '100%';
        document.getElementById("p2").style.width = '';
        document.getElementById("p2").style.bottom = '';
        document.getElementById("p2").style.transition = "";
        document.getElementById("p2").style.background = "";
    }*/



    render() {
        return(
            <div className="container">
                <div className="container">
                    <div className="row justify-content-md-center">
                        <div className="col-md-6 text-center">
                            <div className="row">
                                <div className="col padleft">
                                    <img onMouseOver={this.changeBack} onMouseOut={this.changeNorm} className="gra1" src="https://image.flaticon.com/icons/svg/1055/1055687.svg" width="128" height="128" id="img1" data-toggle="tooltip" ref="test"
                                         data-placement="left" title="TECH"></img>
                                        <img src="https://image.flaticon.com/icons/svg/1055/1055648.svg" width="128" height="128"id="img2" data-toggle="tooltip" ref="test"
                                             data-placement="left" title="BIZDEV"></img>
                                </div>
                                <div className="col padright">
                                    <img src="https://image.flaticon.com/icons/svg/1055/1055662.svg" width="128" height="128"id="img3" data-toggle="tooltip" ref="test"
                                         data-placement="right" title="DESIGN"></img>
                                        <img src="https://image.flaticon.com/icons/svg/1055/1055669.svg" width="128" height="128"id="img4" data-toggle="tooltip" ref="test"
                                             data-placement="right" title="MULTIMEDIA"></img>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default IconImage;